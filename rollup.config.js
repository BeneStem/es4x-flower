import nodePolyfills from 'rollup-plugin-polyfill-node';
import commonjs from '@rollup/plugin-commonjs';
import nodeResolve from '@rollup/plugin-node-resolve';
import babel from '@rollup/plugin-babel';
import postcss from 'rollup-plugin-postcss-modules';
import { terser } from 'rollup-plugin-terser';

export default [
  {
    input: 'src/server/Server.ts',
    output: {
      file: 'dist/server/Server.js',
      format: 'esm',
    },
    external: [
      '@vertx/core',
      '@vertx/web',
      '@vertx/pg-client',
      '@vertx/health-check',
      '@vertx/web-validation',
      '@vertx/micrometer-metrics',
    ],
    plugins: [
      nodePolyfills(),
      commonjs(),
      nodeResolve({
        extensions: ['.ts', '.tsx', '.js', '.jsx'],
        exportConditions: ['solid', 'node'],
      }),
      babel({
        extensions: ['.ts', '.tsx', '.js', '.jsx'],
        babelHelpers: 'bundled',
        presets: [['solid', { generate: 'ssr', hydratable: true }], '@babel/typescript'],
      }),
      postcss({
        extract: 'public/Server.css',
        modules: true,
        minimize: true,
        writeDefinitions: true,
      }),
    ],
    preserveEntrySignatures: false,
  },
  {
    input: 'src/client/Client.tsx',
    output: [
      {
        file: 'dist/server/public/Client.js',
        format: 'esm',
      },
    ],
    external: [
      '@vertx/core',
      '@vertx/web',
      '@vertx/pg-client',
      '@vertx/health-check',
      '@vertx/web-validation',
      '@vertx/micrometer-metrics',
      'sanitize-html',
      'slug',
      'validator',
    ],
    plugins: [
      commonjs(),
      nodeResolve({
        extensions: ['.ts', '.tsx', '.js', '.jsx'],
        exportConditions: ['solid'],
      }),
      babel({
        extensions: ['.ts', '.tsx', '.js', '.jsx'],
        babelHelpers: 'bundled',
        presets: [
          ['@babel/preset-env', { targets: ['> 1%', 'last 2 versions', 'not dead'] }],
          ['solid', { generate: 'dom', hydratable: true }],
          '@babel/typescript',
        ],
      }),
      postcss({ inject: false, modules: true }),
      terser(),
    ],
    preserveEntrySignatures: false,
  },
];
