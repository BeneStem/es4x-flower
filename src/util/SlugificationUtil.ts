import slug from 'slug';

export class SlugificationUtil {
  static slugify(text: string): string {
    slug.defaults.mode = 'rfc3986';
    slug.extend({
      '(': '-',
      ')': '-',
      '[': '-',
      ']': '-',
      '{': '-',
      '}': '-',
      '.': '-',
    });
    return slug(text, { remove: /[~]/g, locale: 'de' });
  }
}
