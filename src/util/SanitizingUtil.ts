import trim from 'validator/es/lib/trim';
import unescape from 'validator/es/lib/unescape';
import sanitize from 'sanitize-html';

export class SanitizingUtil {
  static sanitize(text: string): string {
    const sanitizingOptions = {
      allowedTags: [],
      allowedAttributes: {},
    };
    return trim(unescape(sanitize(unescape(text), sanitizingOptions)));
  }
}
