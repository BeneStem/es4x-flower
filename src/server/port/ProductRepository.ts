import { Product } from '../domain/model/Product';

export interface ProductRepository {
  findProducts(): Promise<Product[]>;

  saveProduct(product: Product): Promise<Product>;
}
