import { InvalidProductName, ProductName } from './ProductName';
// TODO find a way to get rid of this from the domain
import { HttpProduct } from '../../../app/adapter/HttpProduct';

export class Product {
  name: ProductName;

  private constructor(name: ProductName) {
    this.name = Object.freeze(name);
  }

  static create(name: ProductName): Product {
    return new Product(name);
  }

  static fromHttpProduct(httpProduct: HttpProduct): Product | InvalidProduct {
    const productName = ProductName.create(httpProduct.name);
    return productName instanceof ProductName
      ? Product.create(productName)
      : InvalidProduct.create(productName);
  }
}

export class InvalidProduct {
  invalidName: InvalidProductName;

  private constructor(invalidName: InvalidProductName) {
    this.invalidName = invalidName;
  }

  static create(invalidName: InvalidProductName): InvalidProduct {
    return new InvalidProduct(invalidName);
  }
}
