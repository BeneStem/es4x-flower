import isAlpha from 'validator/es/lib/isAlpha';
import isLength from 'validator/es/lib/isLength';

export class ProductName {
  readonly value: string;

  private constructor(value: string) {
    this.value = value;
  }

  static create(name: string): ProductName | InvalidProductName {
    const alpha = isAlpha(name, 'de-DE');
    const length = isLength(name, { min: 1, max: 20 });
    return alpha && length ? new ProductName(name) : InvalidProductName.create();
  }
}

export class InvalidProductName {
  static create(): InvalidProductName {
    return new InvalidProductName();
  }
}
