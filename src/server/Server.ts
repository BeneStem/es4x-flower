import { PgConnectOptions } from '@vertx/pg-client/options';
import { PoolOptions } from '@vertx/sql-client/options';
import { PgPool } from '@vertx/pg-client';
import { ServerRouter } from './ServerRouter';

const connectOptions = new PgConnectOptions()
  .setCachePreparedStatements(true)
  .setHost('localhost')
  .setDatabase('postgres')
  .setUser('es4x-flower')
  .setPassword('es4x-flower');
const poolOptions = new PoolOptions().setMaxSize(8);
const pgClient = PgPool.pool(vertx, connectOptions, poolOptions);

vertx
  .createHttpServer()
  .requestHandler(ServerRouter.create(vertx, pgClient))
  .listen(3000)
  .then(
    () => {
      console.log('Server started on port 3000');
    },
    () => {
      console.log('ERROR!');
    }
  );
