import { Vertx } from '@vertx/core';
import { PgPool } from '@vertx/pg-client';
import { Router as VertxRouter, RoutingContext, StaticHandler } from '@vertx/web';
import { HealthCheckHandler } from '@vertx/health-check';
import { Status } from '@vertx/health-check/options';
import { ProductApiRouter } from './adapter/active/ProductApiRouter';
import { HydrationScript, renderToString, renderToStringAsync } from 'solid-js/web';
import App from '../app/adapter/active/App';

export class ServerRouter {
  static create(vertx: Vertx, pgClient: PgPool): VertxRouter {
    const serverRouter = VertxRouter.router(vertx);

    serverRouter.route().handler(StaticHandler.create(`dist/server/public`));

    // TODO move to own router/controller
    const healthCheckHandler = HealthCheckHandler.create(vertx);
    healthCheckHandler.register('database', (promise) =>
      pgClient.getConnection((connection) => {
        if (connection.succeeded()) {
          connection.result()?.close();
          const status = new Status();
          status.setOk(true);
          promise.complete(status);
        }
      })
    );
    serverRouter.get('/health').handler(healthCheckHandler);

    serverRouter.route('/api/products*').subRouter(ProductApiRouter.create(vertx, pgClient));

    serverRouter.get('/*').handler(async (req: RoutingContext) => {
      const app = await renderToStringAsync(() => (
        <App startLocation={req.normalizedPath()} pgClient={pgClient} />
      ));

      const body = `<html lang='en'>
    <head>
        <title>🔥 Solid SSR 🔥</title>
        <meta charset='UTF-8' />
        ${renderToString(() => <HydrationScript />)}
        <link rel='stylesheet' href='/Server.css' media='all'>
    </head>
    <body>
        <div id='app'>${app}</div>
    </body>
    <script type='module' src='/Client.js'></script>
</html>`;

      await req.response().putHeader('content-type', 'text/html').end(body);
    });

    return serverRouter;
  }
}
