import { ProductPostgresRepository } from '../adapter/passive/ProductPostgresRepository';
import { PgPool } from '@vertx/pg-client';
import { Product } from '../domain/model/Product';

export class ProductApplicationService {
  private productPostgresRepository: ProductPostgresRepository;

  constructor(pgClient: PgPool) {
    this.productPostgresRepository = new ProductPostgresRepository(pgClient);
  }

  findProducts(): Promise<Product[]> {
    return this.productPostgresRepository.findProducts();
  }

  saveProduct(product: Product): Promise<Product> {
    return this.productPostgresRepository.saveProduct(product);
  }
}
