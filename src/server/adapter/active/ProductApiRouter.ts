import { Vertx } from '@vertx/core';
import { PgPool } from '@vertx/pg-client';
import { BodyHandler, Router, RoutingContext } from '@vertx/web';
import { ProductApplicationService } from '../../application/ProductApplicationService';
import { HttpProduct } from '../../../app/adapter/HttpProduct';
import { Product } from '../../domain/model/Product';

export class ProductApiRouter {
  static create(vertx: Vertx, pgClient: PgPool): Router {
    const productApplicationService = new ProductApplicationService(pgClient);

    const productApiRouter = Router.router(vertx);

    productApiRouter.get('/').handler(ProductApiRouter.getProducts(productApplicationService));
    productApiRouter
      .post('/')
      .handler(BodyHandler.create())
      .handler(ProductApiRouter.saveProduct(productApplicationService));

    return productApiRouter;
  }

  private static getProducts(productApplicationService: ProductApplicationService) {
    return async (req: RoutingContext) => {
      const products = await productApplicationService.findProducts();
      const restProducts = products.map((product) => HttpProduct.fromProduct(product));
      await req
        .response()
        .putHeader('content-type', 'application/json')
        .end(JSON.stringify(restProducts));
    };
  }

  private static saveProduct(productApplicationService: ProductApplicationService) {
    return async (req: RoutingContext) => {
      // TODO add validation of body?
      const product = Product.fromHttpProduct(req.getBodyAsJson() as HttpProduct);
      if (product instanceof Product) {
        const returningProduct = await productApplicationService.saveProduct(product);
        await req
          .response()
          .putHeader('content-type', 'application/json')
          .end(JSON.stringify(HttpProduct.fromProduct(returningProduct)));
      }
      await req.response().setStatusCode(400).end();
    };
  }
}
