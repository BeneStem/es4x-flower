import { ProductRepository } from '../../port/ProductRepository';
import { PgPool } from '@vertx/pg-client';
import { Product } from '../../domain/model/Product';
import { ProductName } from '../../domain/model/ProductName';
import { nameof } from 'ts-simple-nameof';
import { Tuple } from '@vertx/sql-client';

export class ProductPostgresRepository implements ProductRepository {
  private pgClient: PgPool;

  constructor(pgClient: PgPool) {
    this.pgClient = pgClient;
  }

  findProducts(): Promise<Product[]> {
    return new Promise((resolve) => {
      this.pgClient.preparedQuery('SELECT * FROM products').execute((asyncResult) => {
        const products: Product[] = [];

        if (asyncResult.succeeded()) {
          // TODO stream result async/await
          const rows = asyncResult.result()?.iterator();
          while (rows?.hasNext()) {
            const row = rows.next();
            const productName = ProductName.create(row.getString(nameof<Product>((p) => p.name)));
            if (productName instanceof ProductName) {
              products.push(Product.create(productName));
            }
          }
        }

        resolve(products);
      });
    });
  }

  saveProduct(product: Product): Promise<Product> {
    return new Promise((resolve, reject) => {
      this.pgClient
        .preparedQuery('INSERT INTO products (name) VALUES ($1) RETURNING *')
        .execute(Tuple.of(product.name.value), (asyncResult) => {
          if (asyncResult.succeeded() && asyncResult.result()?.rowCount() === 1) {
            const rows = asyncResult.result()?.iterator();
            if (rows?.hasNext()) {
              const row = rows.next();
              const productName = ProductName.create(row.getString(nameof<Product>((p) => p.name)));
              if (productName instanceof ProductName) {
                resolve(Product.create(productName));
              }
            }
          } else {
            reject(asyncResult.cause()?.getMessage());
          }
        });
    });
  }
}
