import { ViewProductRepository } from '../../../app/port/ViewProductRepository';
import { ProductApplicationService } from '../../application/ProductApplicationService';
import { ViewProduct } from '../../../app/domain/model/ViewProduct';
import { PgPool } from '@vertx/pg-client';

export class ViewProductApplicationRepository implements ViewProductRepository {
  private productApplicationService: ProductApplicationService;

  constructor(pgClient: PgPool) {
    this.productApplicationService = new ProductApplicationService(pgClient);
  }

  findViewProducts(): Promise<ViewProduct[]> {
    return this.productApplicationService
      .findProducts()
      .then((products) => products.map((product) => ViewProduct.fromProduct(product)));
  }
}
