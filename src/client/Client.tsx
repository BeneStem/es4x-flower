import { hydrate } from 'solid-js/web';
import App from '../app/adapter/active/App';

hydrate(() => <App />, document.getElementById('app') ?? document);
