import { ViewProductRepository } from '../../../app/port/ViewProductRepository';
import { ViewProduct } from '../../../app/domain/model/ViewProduct';
import { HttpProduct } from '../../../app/adapter/HttpProduct';

export class ViewProductRestRepository implements ViewProductRepository {
  findViewProducts(): Promise<ViewProduct[]> {
    return fetch('/api/products')
      .then((response) => response.json() as unknown as Promise<HttpProduct[]>)
      .then((httpProducts) =>
        httpProducts.map((httpProduct) => ViewProduct.fromHttpProduct(httpProduct))
      );
  }
}
