import { Product } from '../../server/domain/model/Product';

export class HttpProduct {
  name: string;

  private constructor(name: string) {
    this.name = name;
  }

  static create(name: string): HttpProduct {
    return new HttpProduct(name);
  }

  static fromProduct(product: Product): HttpProduct {
    return HttpProduct.create(product.name.value);
  }
}
