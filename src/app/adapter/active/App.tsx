import { Component, createSignal, Switch } from 'solid-js';
import { PgPool } from '@vertx/pg-client';
import { isServer } from 'solid-js/web';
import { Link, MatchRoute, pathIntegration, Router } from '@rturnq/solid-router';
import HomePage from './HomePage';
import ProductListPage from './product/ProductListPage';

const App: Component<{ startLocation?: string; pgClient?: PgPool }> = (props) => {
  const integration = isServer
    ? createSignal({ value: props.startLocation ?? '' })
    : pathIntegration();
  return (
    <Router integration={integration}>
      <nav>
        <Link href="/">Home</Link>
        <Link href="/products">Products</Link>
      </nav>
      <main>
        <Switch fallback={<h1>404</h1>}>
          <MatchRoute end>
            <HomePage />
          </MatchRoute>
          <MatchRoute path="products">
            <ProductListPage pgClient={props.pgClient} />
          </MatchRoute>
        </Switch>
      </main>
    </Router>
  );
};

export default App;
