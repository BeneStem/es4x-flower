import { Component } from 'solid-js';
import style from './HomePage.pcss';

const HomePage: Component = () => {
  return <h1 class={style.welcomeHeadline}>Welcome!</h1>;
};

export default HomePage;
