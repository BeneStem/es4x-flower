import { Component, For } from 'solid-js';
import { ViewProduct } from '../../../domain/model/ViewProduct';
import style from './ProductList.pcss';

const ProductList: Component<{ products: ViewProduct[] }> = (props) => (
  <For each={props.products}>
    {(product: ViewProduct) => <p class={style.productName}>{product.name}</p>}
  </For>
);

export default ProductList;
