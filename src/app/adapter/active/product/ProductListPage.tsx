import { Component, createResource, Suspense } from 'solid-js';
import { PgPool } from '@vertx/pg-client';
import { ViewProduct } from '../../../domain/model/ViewProduct';
import { ViewProductApplicationService } from '../../../application/ViewProductApplicationService';
import ProductList from './ProductList';

const ProductListPage: Component<{ pgClient?: PgPool }> = (props) => {
  const [products] = createResource((): Promise<ViewProduct[]> => {
    return ViewProductApplicationService.findViewProducts(props.pgClient!);
  });

  return (
    <>
      <h1>Products</h1>
      <Suspense>
        <ProductList products={products()!} />
      </Suspense>
    </>
  );
};

export default ProductListPage;
