import { HttpProduct } from '../../adapter/HttpProduct';
import { Product } from '../../../server/domain/model/Product';

export class ViewProduct {
  name: string;

  private constructor(name: string) {
    this.name = name;
  }

  static create(name: string): ViewProduct {
    return new ViewProduct(name);
  }

  static fromHttpProduct(httpProduct: HttpProduct): ViewProduct {
    return ViewProduct.create(httpProduct.name);
  }

  static fromProduct(product: Product): ViewProduct {
    return ViewProduct.create(product.name.value);
  }
}
