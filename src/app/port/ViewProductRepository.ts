import { ViewProduct } from '../domain/model/ViewProduct';

export interface ViewProductRepository {
  findViewProducts(): Promise<ViewProduct[]>;
}
