import { ViewProductApplicationRepository } from '../../server/adapter/passive/ViewProductApplicationRepository';
import { ViewProductRestRepository } from '../../client/adapter/passive/ViewProductRestRepository';
import { PgPool } from '@vertx/pg-client';
import { ViewProduct } from '../domain/model/ViewProduct';
import { isServer } from 'solid-js/web';

export class ViewProductApplicationService {
  static findViewProducts(pgClient: PgPool): Promise<ViewProduct[]> {
    if (isServer) {
      const viewProductApplicationRepository = new ViewProductApplicationRepository(pgClient);
      return viewProductApplicationRepository.findViewProducts();
    } else {
      const viewProductRestRepository = new ViewProductRestRepository();
      return viewProductRestRepository.findViewProducts();
    }
  }
}
