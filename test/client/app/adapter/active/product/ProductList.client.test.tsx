import '@testing-library/jest-dom/extend-expect';
import { render } from 'solid-testing-library';
import ProductList from '../../../../../../src/app/adapter/active/product/ProductList';
import { ViewProduct } from '../../../../../../src/app/domain/model/ViewProduct';

describe('ProductList', () => {
  it('should render', () => {
    const {
      container: { firstChild: productList },
    } = render(() => <ProductList products={[ViewProduct.create('TestProduct')]} />);
    expect(productList).toHaveTextContent('TestProduct');
  });
});
