import { SlugificationUtil } from '../../src/util/SlugificationUtil';

describe('SlugificationUtil', () => {
  it('should replace umlauts', () => {
    const slugifyText = SlugificationUtil.slugify('Schöne neue Röhrenjeans in Größe 42');
    expect(slugifyText).toBe('schoene-neue-roehrenjeans-in-groesse-42');
  });

  it('should replace special characters', () => {
    const slugifyText = SlugificationUtil.slugify("Haup(t)hose_+*~#'/-\"'un[d]so--Wahns{i}n.n;");
    expect(slugifyText).toBe('haup-t-hose_-un-d-so-wahns-i-n-n');
  });

  it('should return empty string for empty text', () => {
    const slugifyText = SlugificationUtil.slugify('');
    expect(slugifyText).toBe('');
  });
});
