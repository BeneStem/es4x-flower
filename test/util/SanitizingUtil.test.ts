import { SanitizingUtil } from '../../src/util/SanitizingUtil';

describe('SanitizingUtil', () => {
  it('should trim', () => {
    const sanitizedText = SanitizingUtil.sanitize('    i will be remaining   ');
    expect(sanitizedText).toBe('i will be remaining');
  });

  it('should remove empty content', () => {
    const sanitizedText = SanitizingUtil.sanitize('<div> </div>');
    expect(sanitizedText).toBe('');
  });

  it('should keep content', () => {
    const sanitizedText = SanitizingUtil.sanitize('"<b>i will remain</b>"');
    expect(sanitizedText).toBe('"i will remain"');
  });

  it('should not escape', () => {
    const sanitizedText = SanitizingUtil.sanitize('<b>i will & shall remain</b>');
    expect(sanitizedText).toBe('i will & shall remain');
  });

  it('should remove unwanted tags', () => {
    const sanitizedText = SanitizingUtil.sanitize(
      '  <script>i will be completely removed</script>  '
    );
    expect(sanitizedText).toBe('');
  });

  it('should remove unwanted tags in unescaped text', () => {
    const sanitizedText = SanitizingUtil.sanitize('&lt;script&gt;alert(1)&lt;/script&gt;');
    expect(sanitizedText).toBe('');
  });

  it('should return empty string for empty text', () => {
    const sanitizedText = SanitizingUtil.sanitize('');
    expect(sanitizedText).toBe('');
  });
});
