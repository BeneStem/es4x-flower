import { renderToString } from 'solid-js/web';
import ProductList from '../../../../../../src/app/adapter/active/product/ProductList';
import { ViewProduct } from '../../../../../../src/app/domain/model/ViewProduct';

describe('ProductList', () => {
  it('should render', () => {
    const productList = renderToString(() => (
      <ProductList products={[ViewProduct.create('TestProduct')]} />
    ));
    expect(productList).toBe('<p data-hk="0-0-0" class="productName">TestProduct</p>');
  });
});
