FROM node AS NPM
RUN ln -s /bin/true /usr/bin/es4x
WORKDIR /usr/src/app
COPY . .
RUN npm --unsafe-perm ci --only=prod

FROM adoptopenjdk/openjdk11 AS ES4X
RUN curl -sL https://github.com/reactiverse/es4x/releases/download/0.16.2/es4x-pm-0.16.2-bin.tar.gz | tar zx --strip-components=1 -C /usr/local
COPY --from=NPM /usr/src/app /usr/src/app
WORKDIR /usr/src/app
RUN es4x install --only=prod

FROM adoptopenjdk/openjdk11:jre-11.0.11_9-alpine
COPY --from=ES4X /usr/src/app /usr/src/app
WORKDIR /usr/src/app
ENTRYPOINT [  "java", "--module-path=node_modules/.jvmci", "-XX:+UnlockExperimentalVMOptions", "-XX:+EnableJVMCI", "--upgrade-module-path=node_modules/.jvmci/compiler.jar", "-XX:+UseContainerSupport", "-jar", "./node_modules/.bin/es4x-launcher.jar" ]
