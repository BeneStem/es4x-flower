## ES4X-FLOWER

[![pipeline status](https://gitlab.com/BeneStem/es4x-flower/badges/master/pipeline.svg)](https://gitlab.com/BeneStem/es4x-flower/-/pipelines)

This project is an example of using [TypeScript](https://www.typescriptlang.org), [ES4X](https://reactiverse.io/es4x/)
and [Solid](https://github.com/ryansolid/solid) to create an ultra fast, typesafe, self contained and universal webapp.

### Why

#### Single-page-application

Using a single-page-application as a base for ones webapp is mandatory these days. Without them you wont be able to
achieve the expected user experience. And on top they open up the possibility to create a progressive web or hybrid app
for any mobile device.

#### Server-side-rendering

But to create a fast single-page-application fulfilling all requirements, like SEO, its necessary to use
server-side-rendering. For doing so you need to provide a runtime able to interpret JavaScript code like Node.js. Common
webservers based on Node.js are [express](https://expressjs.com), [fastify](https://www.fastify.io) or others. But all
of them do not perform very well compared to other webservers, like shown in
this [benchmark](https://www.techempower.com/benchmarks/#section=data-r20&hw=ph&test=fortune&l=zik0sf-sd).

The fastest JavaScript webserver in the benchmark is [ES4X](https://reactiverse.io/es4x/). Which is not very surprising,
because it is **not**
running on Node.js but [GraalVM](https://www.graalvm.org) or the JVM.

[GraalVM](https://www.graalvm.org) is
a [polyglot compiler](https://www.graalvm.org/reference-manual/polyglot-programming/) making it possible to run
JavaScript on the JVM. [ES4X](https://reactiverse.io/es4x/) itself is a JavaScript API binding
of [Vert.x](https://vertx.io) which provides one of the fastest JVM webservers.

But [ES4X](https://reactiverse.io/es4x/) cant run all single-page-application frameworks. Mostly because a lot of them
require specific Node.js APIs which are not available when running on [GraalVM](https://www.graalvm.org). It is possible
to use polyfills for some of them but those are not working for mayor SPAs like [VueJS](https://vuejs.org)
or [Angular](https://angular.io). It is possible to server side render [React](https://reactjs.org)
but its not known for being very fast.

As shown in this [SPA benchmark](https://krausest.github.io/js-framework-benchmark/2021/table_chrome_91.0.4472.77.html)
the fastest one out there is [Solid](https://github.com/ryansolid/solid). And as the matter of fact, due to its
minimalistic approach it is possible to server-side-render it on [GraalVM](https://www.graalvm.org).

#### Database connectivity

Another important aspect of a webapp is a connection to a database. Using Node.js as a runtime can proof inefficient
when doing so. Or in most cases requires a web-client for connecting to a backend during server-side-rendering other
than a database-client. But we are in luck, [Vert.x](https://vertx.io) provides extremely fast
database [clients](https://vertx.io/docs/vertx-pg-client/java/) for all the mayor databases that can be used
with [Solid](https://github.com/ryansolid/solid).

This results in the possibility to use the database directly when server-side-rendering and an API in the client. Which
makes the app even faster in comparison to other solutions.

#### Domain Driven Design

Domain Driven Design requires to implement a domain very strictly using types. JavaScript has types but they are highly
dynamic and you cant define or declare types. Using TypeScript makes up for that defect.

[TypeScript](https://www.typescriptlang.org) and [Vert.x](https://vertx.io) combined suite Domain Driven Design very
well.

#### Self contained system

Architecture-wise it is very complicated building a single-page-application. They tend to lead to architectures that
have a lot of layers and extra build steps compared to simply using good old HTML, rendered from a webserver.

You need a backend-server with most of the time a database connection, providing a REST API to be used client-side.
After server-side-rendering the SPA you need to hydrate it in the client. The SPA itself will consist of layers for UI,
business logic and connection to the backend.

Often the backend-server is even written in a different language resulting in two runtimes, one for the backend and one
for server-side-rendering the SPA.

Using the [TypeScript](https://www.typescriptlang.org), [ES4X](https://reactiverse.io/es4x/)
and [Solid](https://github.com/ryansolid/solid) approach gets of these downsides and makes it possible to write all of
that in a single, self contained system. With a little bit of bundling magic you can even reuse most parts of your code
for both client and server side.

### Key Features

Having heard why I build this, here is a list of features included in this example:

- Git Hooks with [husky](https://typicode.github.io/husky/#/)
- Commit Linting with [commitzen](https://commitizen-tools.github.io/commitizen/index.html)
- Bundling with [rollup](https://rollupjs.org)
- Linting with [eslint](http://eslint.org)
- Formatting with [prettier](http://prettier.io)
- Testing with [jest](http://jestjs.io)
- Coverage with [jest](http://jestjs.io)
- Dependency Checking with [npm-check-updates](https://www.npmjs.com/package/npm-check-updates)
- [Storybook](http://storybook.js.org)
- Container Image with [docker](https://www.docker.com)
- Watch Mode with [customized script](https://gitlab.com/BeneStem/es4x-flower/-/blob/master/bin/watch.js)
- Server-side-rendering
  done [here](https://gitlab.com/BeneStem/es4x-flower/-/blob/master/src/server/ServerRouter.tsx#L33)
- Hydration provided [here](https://gitlab.com/BeneStem/es4x-flower/-/blob/master/src/server/ServerRouter.tsx#L41) and
  used [here](https://gitlab.com/BeneStem/es4x-flower/-/blob/master/src/client/Client.tsx#L4)
- Postgres Connection created [here](https://gitlab.com/BeneStem/es4x-flower/-/blob/master/src/server/Server.ts#L13)
- Router using [@rturnq/solid-router](https://github.com/rturnq/solid-router)
- Validation using [validator](https://www.npmjs.com/package/validator)
- Sanitizing using [sanitize-html](https://www.npmjs.com/package/sanitize-html)
  and [validator](https://www.npmjs.com/package/validator)
- Slugification using [slug](https://www.npmjs.com/package/slug)
- CSS Modules using [rollup-plugin-postcss-modules](https://www.npmjs.com/package/rollup-plugin-postcss-modules)

## Quick Start

Now you probably want to try it for yourself.

Install requirements for ES4X:

- Just follow [this documentation](https://reactiverse.io/es4x/get-started/install.html#install) to install Node.js and
  Java.

Afterwards clone and install the example:

```shell
git clone git@gitlab.com:BeneStem/es4x-flower.git
cd TOOD
npm ci
```

Run the example:

```shell
npm start
```

This will probably make it start but not render any Produkte as they need a running postgresql process containing data:

First install postgres on your system. There are multiple ways described [here](https://www.postgresql.org/download/).

After that connect to your local instance:

```shell
psql postgres
```

Create a user with enough rights, a table for products and add some:

```sql
CREATE USER "es4x-flower" WITH PASSWORD 'es4x-flower';

ALTER USER "es4x-flower" WITH SUPERUSER;

CREATE TABLE products
(
  id   SERIAL PRIMARY KEY,
  name VARCHAR(200) NOT NULL
);

INSERT INTO products(name)
VALUES ('Käse');

INSERT INTO products(name)
VALUES ('Schinken');

INSERT INTO products(name)
VALUES ('Brot');
```

### More commands

`Cleaning the build directory`

```shell
npm run clean
```

`Building the assets`

```shell
npm run build
```

`Linting the code`

```shell
npm run lint
```

`Testing the code`

```shell
npm test
```

`Coverage report`

```shell
npm run coverage
```

`Updating dependencies`

```shell
npm run dependency-updates # to list the possible updates
npm run dependency-updates -- -u # to update them in package.json
npm install # to install the updates
```

`Start Storybook during development`

```shell
npm run start-storybook
```

`Build the static Storybook for release`

```shell
npm run build-storybook
```

### Test the performance

If you want to test the performance, simply start up the webapp and use the cli [hey](https://github.com/rakyll/hey) to
run a few requests against it.

## Documentation

- ES4X
  - [Advanced](https://reactiverse.io/es4x/advanced/)
- Vert.x
  - [Docs](https://vertx.io/docs/)
  - [Core](https://vertx.io/docs/vertx-core/java/)
  - [Web](https://vertx.io/docs/vertx-web/java/)
  - [Web Validation](https://vertx.io/docs/vertx-web-validation/java/)
  - [PostgreSQL Client](https://vertx.io/docs/vertx-pg-client/java/)
  - [Health Check](https://vertx.io/docs/vertx-health-check/java/)
  - [Micrometer Metrics](https://vertx.io/docs/vertx-micrometer-metrics/java/)
  - [Web Client](https://vertx.io/docs/vertx-web-client/java/)
- Solid
  - [Getting Started](https://github.com/solidjs/solid/blob/main/documentation/guides/getting-started.md)
  - [Reactivity](https://github.com/solidjs/solid/blob/main/documentation/guides/reactivity.md)
  - [Rendering](https://github.com/solidjs/solid/blob/main/documentation/guides/rendering.md)
  - [SSR](https://github.com/solidjs/solid/blob/main/documentation/guides/server.md)

### Examples

- ES4X
  - [Examples](https://reactiverse.io/es4x/examples/)
- Solid
  - [Examples](https://github.com/solidjs/solid/blob/main/documentation/resources/examples.md)

## Next Steps

- Create custom gitlab image with node and java
- I18n
- Environment specific properties
- State Management
- Feature Flags (Unleash)
- Metrics (server & client)
- Web request validation
- Fault tolerance
- Logging (client)
- OpenAPI
- Authentication
