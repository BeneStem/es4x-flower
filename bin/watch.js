const loadConfigFile = require('../node_modules/rollup/dist/loadConfigFile');
const path = require('path');
const rollup = require('rollup');
const { spawn, exec } = require('child_process');

let server;

loadConfigFile(path.resolve(__dirname, '../rollup.config.js'), { format: 'es' }).then(
  ({ options, warnings }) => {
    const watcher = rollup.watch(options);

    watcher.on('event', (event) => {
      if (event.code === 'START' && server) {
        server.on('exit', () => {
          server = undefined;
        });
        exec(`kill -- -${server.pid}`);
      }
      if (event.code === 'END' && !server) {
        server = spawn('es4x', ['./dist/server/Server.js'], {
          detached: true,
          cwd: process.cwd(),
          env: process.env,
          stdio: 'inherit',
        });
      }
      if (event.code === 'BUNDLE_END' && event.result) {
        event.result.close();
      }
    });
  }
);

function stopServer() {
  if (server) {
    exec(`kill -- -${server.pid}`);
  }
  process.exit();
}

[`exit`, `SIGINT`, `SIGUSR1`, `SIGUSR2`, `uncaughtException`, `SIGTERM`].forEach((eventType) => {
  process.on(eventType, stopServer);
});
